namespace RelayCommandSample
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Input;

    public class ViewModel : INotifyPropertyChanged
    {
        private int value;

        public ViewModel()
        {
            this.IncrementCommand = new RelayCommand(
                _ => this.Value++,
                _ => this.Value < 10);

            this.DecrementCommand = new RelayCommand(
                _ => this.Value--,
                _ => this.Value > -10);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand IncrementCommand { get; }

        public ICommand DecrementCommand { get; }

        public int Value
        {
            get => this.value;

            set
            {
                if (value == this.value)
                {
                    return;
                }

                this.value = value;
                this.OnPropertyChanged();
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}